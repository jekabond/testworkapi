package com.evgenbondarenko.testworkprivat.presentational;

import android.util.Log;


import com.evgenbondarenko.testworkprivat.data.model.ModelApi;
import com.evgenbondarenko.testworkprivat.domain.Interactor;
import com.evgenbondarenko.testworkprivat.domain.InteractorContract;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class MainPresenter implements MainContract.Presenter {
    private MainContract.View view;
    private CompositeDisposable disposable;
    private InteractorContract interactor;
    private Map<String, ModelApi> map;

    public MainPresenter() {
        disposable = new CompositeDisposable();
        interactor = new Interactor();
    }

    @Override
    public void init() {
        map = new HashMap<>();
        Disposable dispos = interactor.getDataApi()
                .doOnError(Throwable::getMessage)
                .subscribe(this::updateList, throwable -> Log.d("DEBUG", "error get all news"));
        disposable.add(dispos);
    }


    @Override
    public void updateList(List<ModelApi> data) {
//        Iterator<ModelApi> iterator = data.iterator();
//        while (iterator.hasNext()) {
//            ModelApi model = iterator.next();
//            map.put(model.getCcy(), model);
//        }
        if (data != null) view.UpdateDataCourse(data);
        else view.viewTextToast("Нет свзяи с сервером");
    }

    @Override
    public void calc(List<ModelApi> data, String v, String number, boolean b) {
        String course = "";
        String resultView = "";
        String valuta = "";
        double result = 0;
        double numb = Double.valueOf(number);
        switch (v) {
            case "USD":
                if (b) course = (data.get(0).getBuy());
                else course = (data.get(0).getSale());
                break;
            case "EUR":
                if (b) course = (data.get(1).getBuy());
                else course = (data.get(1).getSale());
                break;
            case "RUB":
                if (b) course = (data.get(2).getBuy());
                else course = (data.get(2).getSale());
                break;
            case "BTC":
                if (b) course = (data.get(3).getBuy());
                else course = (data.get(3).getSale());
                break;
        }
        double c = 0;
        c = Double.parseDouble(course);
        if (b) result = c * numb;
        else result = numb / c;
        switch (v) {
            case "USD":
                if (b) valuta = " UAH";
                else valuta = " USD";
                break;
            case "EUR":
                if (b) valuta = " UAH";
                else valuta = " EUR";
                break;
            case "RUB":
                if (b) valuta = " UAH";
                else valuta = " RUB";
                break;
            case "BTC":
                if (b) valuta = " USD";
                else valuta = " BTC";
                break;
        }
//        resultView = String.valueOf(result).substring(0,8).concat(valuta);
        view.result(String.valueOf(result), valuta);

    }

//    protected String valuta (String v, boolean b){
//
//    }


    @Override
    public void onClick(String v) {
        view.CheckSwitch(v);
    }


    @Override
    public void starView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        if (view != null) view = null;
        if (disposable != null && !disposable.isDisposed()){
            disposable.dispose();
        }
        interactor = null;
    }
}
