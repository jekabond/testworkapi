package com.evgenbondarenko.testworkprivat.presentational;

import com.evgenbondarenko.testworkprivat.data.model.ModelApi;
import com.evgenbondarenko.testworkprivat.presentational.base.BasePresenter;

import java.util.List;

public interface MainContract {

    interface View {
        void UpdateDataCourse(List<ModelApi> data);

        void CheckSwitch(String v);

        void result(String r, String v);

        void viewTextToast(String t);


    }

    interface Presenter extends BasePresenter<View> {
        void init();

        void updateList(List<ModelApi> data);

        void calc (List<ModelApi> data, String v, String number, boolean b);

        void onClick (String v);
    }
}
