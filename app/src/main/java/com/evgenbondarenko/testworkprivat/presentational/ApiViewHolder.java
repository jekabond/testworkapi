package com.evgenbondarenko.testworkprivat.presentational;

import android.app.LauncherActivity;
import android.content.Context;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.evgenbondarenko.testworkprivat.data.model.ModelApi;
import com.evgenbondarenko.testworkprivat.databinding.ItemApiBinding;

public class ApiViewHolder extends RecyclerView.ViewHolder {
    private ItemApiBinding mBinding;
    private Context mContext;

    public ApiViewHolder(Context context, View itemView) {
        super(itemView);
        mContext = context;
        mBinding = DataBindingUtil.bind(itemView);
    }

    public void bind(ModelApi item){
        if (item != null){
            mBinding.itemValuta.setText(item.getCcy());
            mBinding.itemBuy.setText(item.getBuy());
            mBinding.itemSale.setText(item.getSale());
        }
    }
}
