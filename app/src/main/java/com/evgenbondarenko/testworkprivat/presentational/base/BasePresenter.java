package com.evgenbondarenko.testworkprivat.presentational.base;

public interface BasePresenter<T> {
    void starView(T view);
    void stopView();
}
