package com.evgenbondarenko.testworkprivat.presentational;

import android.os.Bundle;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.evgenbondarenko.testworkprivat.R;
import com.evgenbondarenko.testworkprivat.data.model.ModelApi;
import com.evgenbondarenko.testworkprivat.databinding.ActivityMainBinding;
import com.evgenbondarenko.testworkprivat.presentational.base.BaseActivity;
import com.evgenbondarenko.testworkprivat.presentational.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {
    private MainContract.Presenter presenter;
    private Adapter adapter;




//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        presenter = new MainPresenter();
//        getBinding().setEvent(presenter);
//        presenter.starView(this);
//        presenter.init();
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        getBinding().rvSignalApi.setLayoutManager(layoutManager);
//        adapter = new Adapter(new ArrayList<>());
//        getBinding().rvSignalApi.setAdapter(adapter);

//    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = new MainPresenter();
        getBinding().setEvent(presenter);
        presenter.starView(this);
        presenter.init();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        getBinding().rvSignalApi.setLayoutManager(layoutManager);
//        adapter = new Adapter(new ArrayList<>());
//        getBinding().rvSignalApi.setAdapter(adapter);
    }

    @Override
    protected void onStartView() {

    }

    @Override
    protected void onStop() {
        presenter.stopView();
        super.onStop();
    }

    @Override
    protected void onDestroyView() {
        if(presenter != null) presenter = null;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void UpdateDataCourse(List<ModelApi> data) {
        adapter = new Adapter(data);
        getBinding().rvSignalApi.setAdapter(adapter);
//        presenter.dataList(adapter.getmList());

    }

    @Override
    public void CheckSwitch(String v) {
        switch (v) {
            case "USD":
                getBinding().bntUsd.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                getBinding().btnEur.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnRub.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnBtc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;
            case "EUR":
                getBinding().bntUsd.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnEur.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                getBinding().btnRub.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnBtc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;
            case "RUB":
                getBinding().bntUsd.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnEur.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnRub.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                getBinding().btnBtc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;
            case "BTC":
                getBinding().bntUsd.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnEur.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnRub.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                getBinding().btnBtc.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                break;
        }
        String numb = getBinding().numbers.getText().toString();
        if (numb == null || numb.isEmpty()) {

            toast(getResources().getString(R.string.empty_chislo));
            getBinding().bntUsd.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            getBinding().btnEur.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            getBinding().btnRub.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            getBinding().btnBtc.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        } else {
            boolean b;
            if (getBinding().switch1.isChecked()) {
                b = true;
            } else {
                b = false;
            }
            presenter.calc(adapter.getmList(), v, numb, b);
        }
    }

//    @Override
//    public void checkNumber(int i) {
//        presenter.calc(getBinding().numbers.getText().toString());
//    }

    @Override
    public void result(String r, String v) {
        getBinding().tvResult.setText(r);
        getBinding().tvValuta.setText(v);
    }

    @Override
    public void viewTextToast(String t) {
        toast(t);
    }
}
