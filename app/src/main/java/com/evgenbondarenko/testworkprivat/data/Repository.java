package com.evgenbondarenko.testworkprivat.data;

import com.evgenbondarenko.testworkprivat.app.App;
import com.evgenbondarenko.testworkprivat.data.model.ModelApi;

import java.util.List;

import io.reactivex.Single;


public class Repository implements RepositoryContract{
    @Override
    public Single<List<ModelApi>> getData() {
        return App.getRestService().getService().getData();
    }
}
