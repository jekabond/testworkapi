package com.evgenbondarenko.testworkprivat.data.remote;

import com.evgenbondarenko.testworkprivat.data.model.ModelApi;

import java.util.List;

import io.reactivex.Single;

import retrofit2.http.GET;

public interface ApiService {

    @GET("p24api/pubinfo?json&exchange&coursid=5")
    Single<List<ModelApi>> getData();
}
