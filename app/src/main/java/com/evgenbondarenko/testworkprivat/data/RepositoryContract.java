package com.evgenbondarenko.testworkprivat.data;

import com.evgenbondarenko.testworkprivat.data.model.ModelApi;

import java.util.List;

import io.reactivex.Single;

public interface RepositoryContract {

    Single<List<ModelApi>> getData();
}
