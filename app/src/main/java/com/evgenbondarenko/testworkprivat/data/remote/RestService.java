package com.evgenbondarenko.testworkprivat.data.remote;

import com.evgenbondarenko.testworkprivat.BuildConfig;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestService {
    private final ApiService service;
    private final Gson gson;

    private RestService(Builder builder) {
        this.service = builder.service;
        this.gson = builder.gson;
    }

    public ApiService getService() {
        return service;
    }

    public static class Builder {
        private final ApiService service;
        private final Gson gson;

        public Builder(String baseUrl) {
            this.gson = provideGson();
            this.service = provideApiService(gson,provideOkHttpClient(),baseUrl);
        }

        public RestService build() {
            return new RestService(this);
        }

        private OkHttpClient provideOkHttpClient() {
            final OkHttpClient.Builder builder = new OkHttpClient.Builder();

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(logging);
            }
            builder.connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                    .readTimeout(60 * 1000, TimeUnit.MILLISECONDS);

            return builder.build();
        }

        private Gson provideGson() {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
            return gsonBuilder
                    .setLenient()
                    .create();
        }

        private ApiService provideApiService(Gson gson, OkHttpClient okHttpClient, String baseUrl) {
            return createApi(ApiService.class, baseUrl, okHttpClient, GsonConverterFactory.create(gson));
        }


        private static <T> T createApi(Class<T> serviceApi, String baseUrl, OkHttpClient okHttpClient, Converter.Factory factoryConverter) {
            Retrofit.Builder builder = new Retrofit.Builder();
            builder.client(okHttpClient)
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(factoryConverter);
            return builder.build().create(serviceApi);
        }
    }
}
