package com.evgenbondarenko.testworkprivat.domain;

import com.evgenbondarenko.testworkprivat.data.Repository;
import com.evgenbondarenko.testworkprivat.data.RepositoryContract;
import com.evgenbondarenko.testworkprivat.data.model.ModelApi;
import com.evgenbondarenko.testworkprivat.domain.base.BaseInteractor;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;

public class Interactor extends BaseInteractor implements InteractorContract {
    private RepositoryContract repository;

    public Interactor(){
        repository = new Repository();
    }

    @Override
    public Single<List<ModelApi>> getDataApi() {
        return repository.getData()
                .compose(applySingleSchedulers());
    }


}
