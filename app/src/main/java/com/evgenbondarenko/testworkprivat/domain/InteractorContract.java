package com.evgenbondarenko.testworkprivat.domain;

import com.evgenbondarenko.testworkprivat.data.model.ModelApi;


import java.util.List;

import io.reactivex.Single;

public interface InteractorContract {

    Single<List<ModelApi>> getDataApi();
}
