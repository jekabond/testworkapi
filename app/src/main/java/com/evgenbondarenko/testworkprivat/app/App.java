package com.evgenbondarenko.testworkprivat.app;

import android.app.Application;

import com.evgenbondarenko.testworkprivat.BuildConfig;
import com.evgenbondarenko.testworkprivat.data.remote.RestService;

public class App extends Application {
    public static App app;
    private static RestService restService;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        restService = new RestService.Builder(BuildConfig.SERVER_URL).build();
    }

    public static RestService getRestService() {
        return restService;
    }
}
